package eu.unicornH2020.catascopia.probe.metricLibrary;

import eu.unicornH2020.catascopia.probe.exceptions.CatascopiaMetricValueException;

public class CounterMetric extends SimpleMetric<Long> {

	private long cnt;
	private long dstep;
	
	public CounterMetric(String name, String unit, String desc, long minVal, long maxVal, long defaultStep) {
		super(name, unit, desc, true, minVal, maxVal);
		this.cnt = minVal;
		this.dstep = defaultStep;
	}
	
	public CounterMetric(String name, String desc, long minVal, long maxVal, long defaultStep) {
		this(name, "", desc, minVal, maxVal, defaultStep);
	}
	
	public CounterMetric(String name, String desc) {
		this(name, desc, 0, Long.MAX_VALUE, 1);
	}
	
	public void increment() throws CatascopiaMetricValueException {
		this.increment(this.dstep);
	}
	
	public void increment(long step) throws CatascopiaMetricValueException {
		long temp = this.cnt + step;
		
		if ((temp < 0) || (temp > this.getMaxVal().longValue())) //overflow from Long.MAX_VALUE results in negative number 
			throw new CatascopiaMetricValueException("MAX VAL exceeded");
		
		this.cnt += step;
		
		this.setValue(this.cnt);
	}
	
	public void decrement() throws CatascopiaMetricValueException {
		this.decrement(this.dstep);
	}
	
	public void decrement(long step) throws CatascopiaMetricValueException {
		long temp = this.cnt - step;

		if (temp < this.getMinVal().longValue()) 
			throw new CatascopiaMetricValueException("MAX VAL exceeded");
		
		this.cnt -= step;
		
		this.setValue(this.cnt);
	}
	
	public void reset() {
		this.cnt = this.getMinVal().longValue();
	}
	
	public Number getCount() {
		return this.cnt;
	}
}
