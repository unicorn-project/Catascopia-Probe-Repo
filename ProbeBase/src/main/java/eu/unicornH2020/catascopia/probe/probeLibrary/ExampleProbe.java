package eu.unicornH2020.catascopia.probe.probeLibrary;

import java.util.Random;

import eu.unicornH2020.catascopia.probe.Probe;
import eu.unicornH2020.catascopia.probe.exceptions.CatascopiaMetricValueException;
import eu.unicornH2020.catascopia.probe.metricLibrary.CounterMetric;
import eu.unicornH2020.catascopia.probe.metricLibrary.SimpleMetric;
import eu.unicornH2020.catascopia.probe.metricLibrary.TimerMetric;

public class ExampleProbe extends Probe {
	
	private static final int DEFAULT_SAMPLING_PERIOD = 10000;
	private static final String DEFAULT_PROBE_NAME = "ExampleProbe";
	private SimpleMetric<Double> metric1;
	private SimpleMetric<Integer> metric2;
	private CounterMetric metric4;
	
	public ExampleProbe(String name, long period) {
		super(name, period);

		this.metric1 = new SimpleMetric<Double>("m1", "%", "an example of a random percentage", true, 0.0, 100.0);
		this.metric2 = new SimpleMetric<Integer>("m2", "", "an example of a random integer", true, 0, 10);
		
		this.metric4 = new CounterMetric("m4", "an example of a counter metric", 0, 10, 1);

		this.addMetricToProbe(this.metric1);
		this.addMetricToProbe(this.metric2);
		this.addMetricToProbe(this.metric4);
	}
	
	public ExampleProbe() {
		this(DEFAULT_PROBE_NAME, DEFAULT_SAMPLING_PERIOD);
	}
	
	@Override
	public String getDescription() {
		return "An exemplary probe showcasing the offered functionality";
	}
	
	@Override
	public void collect() throws CatascopiaMetricValueException {
		TimerMetric tmetric = new TimerMetric("tmetric", "an example of a timer metric", this.getPeriod());
		this.addMetricToProbe(tmetric);
		tmetric.start();
		
		Random r = new Random();
		
		this.metric1.setValue(r.nextDouble() * 100);
		this.metric2.setValue(r.nextInt(10));
		
		try {
			this.metric4.increment();
		}
		catch(CatascopiaMetricValueException e) {
			e.printStackTrace();
			this.metric4.reset();
		}
		
		try {
			Thread.sleep(r.nextInt(1000));
		} 
		catch (InterruptedException e) {
			e.printStackTrace();
		}
		
		tmetric.finished();
		
//		System.out.println(this.toJSON());
	}
	
	public static void main(String[] args){
		ExampleProbe p = new ExampleProbe();
		p.activate();
	}
}
