package eu.unicornH2020.catascopia.probe;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;

public abstract class Probe extends Thread implements IProbe {
	
	private static final byte MaxErrorCount = 10;

	
	private long period;
	private ProbeStatus status;
	private Logger myLogger;
	private boolean logging;
	private byte errorCount;
	private Map<String, IMetric<?>> metriclist;
	private LinkedBlockingQueue<IMetric<?>> queue;

	
	public Probe(String name, long period) {
		super(name);
		
		this.period = period;
		this.status = ProbeStatus.INACTIVE;
		this.logging = false;
		this.errorCount = 0;
		
		this.metriclist = new HashMap<String, IMetric<?>>();
		
		this.queue = null;

	}
	
	@Override
	public String getProbeName() {
		return super.getName();
	}

	@Override
	public void setProbeName(String name) {
		super.setName(name);
	}

	@Override
	public long getPeriod() {
		return this.period; //return seconds
	}

	@Override
	public void setPeriod(long period) {
		this.period = period;
	}

	@Override
	public ProbeStatus getProbeStatus() {
		return this.status;
	}

	@Override
	public void setProbeStatus(ProbeStatus status) {
		this.status = status;
	}
	
	@Override
	public void addMetricToProbe(IMetric<?> metric) {
		metric.setProbe(this.getName());
		this.metriclist.put(metric.getName(), metric);
	}
	
	@Override
	public Map<String, IMetric<?>> getMetrics() {
		return this.metriclist;
	}


	public List<IMetric<?>> getMetricsAsList() {
		return new ArrayList<IMetric<?>>(this.metriclist.values());
	}
	
	public IMetric<?> getMetric(String name) {
		return this.metriclist.get(name);
	}
	
	@Override
	public void start() {
		this.activate(); //override Thread class start method to call activate if invoked 
	}

	@Override
	public synchronized void activate() {
		if (this.status == ProbeStatus.INACTIVE)
			if (super.isAlive())
				super.notify();
			else
				super.start();
		this.status = ProbeStatus.ACTIVE;
		this.writeToLog(Level.INFO, "Probe Activated");
	}

	@Override
	public void deactivate() {
		this.status = ProbeStatus.INACTIVE;
		this.writeToLog(Level.INFO, "Probe Deactivated");
	}

	@Override
	public synchronized void terminate() {
		this.status = ProbeStatus.TERMINATED;
		this.writeToLog(Level.INFO, "Probe Terminated");
		this.notify();
	}
	
	public void attachLogger(Logger logger) {
		try {
			this.myLogger = logger;
			this.logging = true;
			this.writeToLog(Level.INFO, "logging turned ON");
		}
		catch(Exception e) {
			this.logging = false;
		}
	}
	
	public void writeToLog(Level level, Object msg) {
		if(this.logging)
			this.myLogger.log(level, super.getName() + ">> " + msg);
	}
	
	@Override
	public void run() {
		try {
			while(this.status != ProbeStatus.TERMINATED) {
				if(this.status == ProbeStatus.ACTIVE) {
					try {

						//probe is activated so collect metrics
						this.collect();
												
						if(this.queue != null) {
							this.metriclist.values().forEach(m -> {
								try {
									this.queue.offer(m, 500, TimeUnit.MILLISECONDS);
								} 
								catch (InterruptedException e) {
									this.errorReport();
									this.writeToLog(Level.SEVERE, e);
									e.printStackTrace();
								}
							});
						}
						else
							//debug mode
							System.out.println(this.toJSON());
						
						this.errorCount = 0;
					}
					//TODO monitoring specific errors
					catch(Exception e) {
						this.errorReport();
						this.writeToLog(Level.SEVERE, e);
						e.printStackTrace();
					}
						
					Thread.sleep(this.period);
				}
				else 
					synchronized(this) {
						while(this.status == ProbeStatus.INACTIVE)
							this.wait();
					}
			}
		}
		catch (InterruptedException e) {			
			this.writeToLog(Level.SEVERE, e);
			this.errorReport();
		}
		catch (Exception e) {
			this.writeToLog(Level.SEVERE, e);
			this.errorReport();
		}
	}
	
	public String toJSON() {
		StringBuilder sb = new StringBuilder();
		sb.append("{\"probe\" : \"" + this.getName() + "\",");
		sb.append("\"metrics\":[");
		for(IMetric<?> m : this.metriclist.values())
			sb.append((m.getReport()) ? m.toJSON() + "," : "");
		
		if (!this.metriclist.isEmpty())
			sb.replace(sb.length()-1, sb.length(), "");
		
		sb.append("]}");

		return sb.toString();
	}
	
	public void attachQueue(LinkedBlockingQueue<IMetric<?>> queue) {
		this.queue = queue;
		this.writeToLog(Level.INFO, "Metric Queue attached to Probe");
	}
	
	public void removeQueue() {
		this.queue = null;
		this.writeToLog(Level.INFO, "Metric Queue removed from Probe");
	}
	
	
	private synchronized void errorReport() {
		if ((++this.errorCount) > Probe.MaxErrorCount) {
			this.status = ProbeStatus.TERMINATED;
			this.writeToLog(Level.SEVERE, "terminating due to many errors");
		}
	}

	public void setMetricsAggregatable(boolean b) {
		this.metriclist.forEach((k,m) -> m.setAggregation(b));
	}

}
