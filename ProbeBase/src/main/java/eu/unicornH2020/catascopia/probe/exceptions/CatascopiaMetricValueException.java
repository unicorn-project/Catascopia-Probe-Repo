package eu.unicornH2020.catascopia.probe.exceptions;

public class CatascopiaMetricValueException extends Exception {

	private static final long serialVersionUID = 1L;

	public CatascopiaMetricValueException(String msg) {
		super(msg);
	}
}
