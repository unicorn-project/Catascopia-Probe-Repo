package eu.unicornH2020.catascopia.probe.metricLibrary;

import java.util.Timer;
import java.util.TimerTask;

import eu.unicornH2020.catascopia.probe.exceptions.CatascopiaMetricValueException;

public class TimerMetric extends SimpleMetric<Long> {
	private enum TIMER{IDLE, STARTED, PAUSED, FINISHED};
	
	private TIMER tstate;
	private long tstart;
	private long tval;
	private long maxWaitTime;
	private Timer timer;
	
	public TimerMetric(String name, String desc, long maxWaitTime) {
		super(name, "ms", desc, false, 0L, maxWaitTime);
		
		this.tstate = TIMER.IDLE;
		this.maxWaitTime = maxWaitTime;
		this.tval = 0;
	}
	
	class CatascopiaTimerTask extends TimerTask {
		
		@Override
		public void run() {
			try {
				Thread.sleep(maxWaitTime - tval);
			} 
			catch (InterruptedException e) {
				e.printStackTrace();
			}
		}		
	}
	
	public boolean start() {
		if(this.tstate == TIMER.IDLE) {
			this.tstart = System.currentTimeMillis();
			this.tstate = TIMER.STARTED;
			
			this.timer = new Timer();
			this.timer.schedule(new CatascopiaTimerTask(), 0);
			
			return true;
		}
		return false;
	}
	
	public boolean pause() {
		if(this.tstate == TIMER.STARTED) {
			this.tval = System.currentTimeMillis() - tstart;
			this.tstate = TIMER.PAUSED;

			this.timer.cancel();
			this.timer.purge();
			
			return true;
		}
		return false;
	}
	
	public boolean resume() {
		if(this.tstate == TIMER.PAUSED) {
			this.tstart = System.currentTimeMillis();
			this.tstate = TIMER.STARTED;

			this.timer = new Timer();
			this.timer.schedule(new CatascopiaTimerTask(), 0);
			
			return true;
		}
		return false;
	}
	
	public boolean restart() {
		this.tval = 0;
		this.tstart = System.currentTimeMillis();
		this.tstate = TIMER.STARTED;
		
		this.timer = new Timer();
		this.timer.schedule(new CatascopiaTimerTask(), this.maxWaitTime);
		
		return true;
	}
	
	public boolean finished() throws CatascopiaMetricValueException {
		if(this.tstate == TIMER.STARTED) {
			tval += System.currentTimeMillis() - tstart;
			this.tstate = TIMER.FINISHED;
			
			this.setValue(tval);
			
			this.timer.cancel();
			this.timer.purge();
						
			return true;
		}
		return false;
	}
	
	public String getTimerState() {
		return this.tstate.name();
	}
}
