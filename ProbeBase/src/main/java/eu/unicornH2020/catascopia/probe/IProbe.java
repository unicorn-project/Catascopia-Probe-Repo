package eu.unicornH2020.catascopia.probe;

import java.util.List;
import java.util.Map;
import java.util.concurrent.LinkedBlockingQueue;
import java.util.logging.Level;
import java.util.logging.Logger;

import eu.unicornH2020.catascopia.probe.exceptions.CatascopiaMetricValueException;

public interface IProbe {
	
	public enum ProbeStatus{INACTIVE, ACTIVE, TERMINATED};
	
	public String getProbeName();
	
	public void setProbeName(String name);

	public long getPeriod();
	
	public void setPeriod(long period);
	
	public ProbeStatus getProbeStatus();

	public void setProbeStatus(ProbeStatus status);
	
	public void addMetricToProbe(IMetric<?> metric);
	
	public Map<String, IMetric<?>> getMetrics();

	public List<IMetric<?>> getMetricsAsList();
	
	public IMetric<?> getMetric(String name);

	public void activate();
	
	public void deactivate();
	
	public void terminate();
	
	public void attachLogger(Logger logger);
	
	public void writeToLog(Level level, Object msg);
	
	public void attachQueue(LinkedBlockingQueue<IMetric<?>> queue);
	
	public void removeQueue();
	
	public String toJSON();
	
	public abstract String getDescription();
	
	public abstract void collect() throws CatascopiaMetricValueException;

	public void setMetricsAggregatable(boolean b);
}
