package eu.unicornH2020.catascopia.probe.metricLibrary;

import eu.unicornH2020.catascopia.probe.Metric;
import eu.unicornH2020.catascopia.probe.exceptions.CatascopiaMetricValueException;

public class SimpleMetric<T extends Number> extends Metric<Number> {

	private static final double DEFAULT_AGGREGATION_EWMA_WEIGHTING = 0.65;

	public SimpleMetric(String name, String unit, String desc, boolean higherIsBetter, T minVal, T maxVal) {
		super(name, unit, desc, higherIsBetter, minVal, maxVal);
	}
	
	public void setValue(Number value) throws CatascopiaMetricValueException {
		this.setTimestamp(System.currentTimeMillis());
		if (this.isAggregation())
			super.setValue(this.aggregate(value));
		else
			super.setValue(value);
	}
	
	public Number aggregate(Number val) throws CatascopiaMetricValueException {
		if (val == null || this.getValue() == null)
			return val;
		
		try {
			Double v = Double.valueOf(val.toString());
			Double p = Double.valueOf(this.getValue().toString());
				
			//Exponential Weighted Moving Average 
			v = DEFAULT_AGGREGATION_EWMA_WEIGHTING * v + (1-DEFAULT_AGGREGATION_EWMA_WEIGHTING) * p;		

			//TODO tidy this hack
			if (val instanceof Integer)
				return (Number) val.getClass().cast(v.intValue());
			
			if (val instanceof Long)
				return (Number) val.getClass().cast(v.longValue());
			
			if (val instanceof Float)			
				return (Number) val.getClass().cast(v.floatValue());
			
			return (Number) v;
		}
		catch(Exception e) {
			throw new CatascopiaMetricValueException("Aggregation for metric: " + this.getName() + " failed");
		}
	}
}
