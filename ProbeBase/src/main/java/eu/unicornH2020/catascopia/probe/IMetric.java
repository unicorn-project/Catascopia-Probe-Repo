package eu.unicornH2020.catascopia.probe;

import eu.unicornH2020.catascopia.probe.exceptions.CatascopiaMetricValueException;

public interface IMetric<T> {
	
	public String getName();

	public void setName(String name);

	public String getUnit();

	public void setUnit(String unit);

	public String getType();

	public String getDesc();

	public void setDesc(String desc);

	public long getTimestamp();

	public void setTimestamp(long timestamp);

	public T getValue();

	public void setValue(T value) throws CatascopiaMetricValueException;
	
	public String toJSON();
	
	public boolean isHigherIsBetter();

	public void setHigherIsBetter(boolean higherIsBetter);

	public T getMinVal();

	public void setMinVal(T minVal);

	public T getMaxVal();

	public void setMaxVal(T maxVal);

	public String getProbe();

	public void setProbe(String probe);
	
	public boolean getReport();

	public void setReport(boolean report);
	
	public boolean isAggregation();

	public void setAggregation(boolean aggregation);
}
