package eu.unicornH2020.catascopia.probe;

import eu.unicornH2020.catascopia.probe.exceptions.CatascopiaMetricValueException;

public abstract class Metric<T> implements IMetric<T> {
	
	private static final boolean DEFAULT_AGGREGATION_ENABLED = false;
	
	private String probe;
	private String name;
	private String unit;
	private String desc;
	private long timestamp;
	private boolean higherIsBetter;
	private T value;	
	private T minVal;
	private T maxVal;
	private boolean report;
	
	private boolean aggregation;
	
	
	public Metric(String name, String unit, String desc, boolean higherIsBetter, T minVal, T maxVal) {
		this.name = name;
		this.unit = unit;
		this.desc = desc;
		
		this.higherIsBetter = higherIsBetter;
		this.minVal = minVal;
		this.maxVal = maxVal;
		
		this.value = null;
		this.timestamp = 0L;
		
		this.report =true;
		
		this.aggregation = DEFAULT_AGGREGATION_ENABLED;
	}
	
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getUnit() {
		return unit;
	}

	public void setUnit(String unit) {
		this.unit = unit;
	}

	public String getType() {
		return this.value.getClass().getSimpleName();
	}

	public String getDesc() {
		return desc;
	}

	public void setDesc(String desc) {
		this.desc = desc;
	}

	public long getTimestamp() {
		return timestamp;
	}

	public void setTimestamp(long timestamp) {
		this.timestamp = timestamp;
	}

	public T getValue() {
		return value;
	}

	public void setValue(T value) throws CatascopiaMetricValueException {
		this.value = value;
	}
	
	public boolean isHigherIsBetter() {
		return higherIsBetter;
	}

	public void setHigherIsBetter(boolean higherIsBetter) {
		this.higherIsBetter = higherIsBetter;
	}


	public T getMinVal() {
		return minVal;
	}

	public void setMinVal(T minVal) {
		this.minVal = minVal;
	}

	public T getMaxVal() {
		return maxVal;
	}

	public void setMaxVal(T maxVal) {
		this.maxVal = maxVal;
	}
	
	public String getProbe() {
		return this.probe;
	}

	public void setProbe(String probe){
		this.probe = probe;
	}
	
	public boolean getReport() {
		return report;
	}

	public void setReport(boolean report) {
		this.report = report;
	}

	public boolean isAggregation() {
		return aggregation;
	}

	public void setAggregation(boolean aggregation) {
		this.aggregation = aggregation;
	}

	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("Metric>> name: " + this.name);
		sb.append(", type: " + this.getType());
		sb.append(", unit: " + this.unit);
		sb.append(", desc: " + this.desc);
		sb.append((value == null) ? "" : ", value: " + this.value.toString());
		sb.append((timestamp == 0L) ? "" : ", timestamp: " + this.timestamp);
		sb.append((probe == null) ? "" : ", probe: " + this.probe);
		return sb.toString();
	}
	
	public String toJSON() {
		StringBuilder sb = new StringBuilder();
		sb.append("{");
		sb.append("\"name\":\"" + this.name + "\"");
		sb.append(",\"unit\":\"" + this.unit + "\"");
		sb.append(",\"type\":\"" + this.getType() + "\"");
		sb.append(",\"higherIsBetter\":\"" + Boolean.toString(this.higherIsBetter) + "\"");
		sb.append(",\"minVal\":\"" + this.minVal + "\"");
		sb.append(",\"maxVal\":\"" + this.maxVal + "\"");
		sb.append((value == null) ? "" : ",\"value\":\"" + this.value.toString() + "\"");
		sb.append((timestamp == 0L) ? "" : ",\"timestamp\":\"" + this.timestamp + "\"");
		sb.append((probe == null) ? "" : ",\"probe\":\"" + this.probe + "\"");
		sb.append("}");
		return sb.toString();
	}
}
