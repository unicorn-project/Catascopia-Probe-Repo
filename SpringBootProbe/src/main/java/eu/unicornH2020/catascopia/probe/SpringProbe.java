package eu.unicornH2020.catascopia.probe;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Stream;

import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import eu.unicornH2020.catascopia.probe.Probe;
import eu.unicornH2020.catascopia.probe.exceptions.CatascopiaMetricValueException;
import eu.unicornH2020.catascopia.probe.metricLibrary.SimpleMetric;

public class SpringProbe extends Probe {
	
	private static final int DEFAULT_SAMPLING_PERIOD = 60000;
	private static final String DEFAULT_PROBE_NAME = "SpringProbe";
	
	private static final String DEFAULT_SERVICE_ENDPOINT = "http://localhost";
	private static final String DEFAULT_SERVICE_PORT = "8080";
	private static final String DEFAULT_CATASCOPIA_URI = "/catascopia?raw=true";

	private SimpleMetric<Long> reqSinceUpdate;
	private SimpleMetric<Long> avgThroughput;
	private SimpleMetric<Long> avgResponseTime;
	
	private String service;
	private RestTemplate rest;
	private HttpHeaders headers;
	 
	public SpringProbe(String name, long period) throws IOException {
		super(name, period);
		
		this.reqSinceUpdate = new SimpleMetric<Long>("req_since_update", "", "reporting number of processed requests since last update", true, 0L , Long.MAX_VALUE);
		this.avgThroughput = new SimpleMetric<Long>("avg_throughput", "ops/s", "reporting average throughput per second", true, 0L , Long.MAX_VALUE);
		this.avgResponseTime = new SimpleMetric<Long>("avg_response_time", "ms", "reporting average response time", false, 0L , Long.MAX_VALUE);
		
		this.addMetricToProbe(this.reqSinceUpdate);
		this.addMetricToProbe(this.avgThroughput);
		this.addMetricToProbe(this.avgResponseTime);
				
		this.rest = new RestTemplate();
	    this.headers = new HttpHeaders();
	    this.parseConfig("probe.properties");
	}
	
	public SpringProbe() throws IOException {
		this(DEFAULT_PROBE_NAME, DEFAULT_SAMPLING_PERIOD);
	}

	@Override
	public String getDescription() {
		return "probe reporting monitoring data for Spring framework";
	}

	@Override
	public void collect() throws CatascopiaMetricValueException {
				
		Map<String, Long> updMap = this.getUpdates();
		
		long req = updMap.getOrDefault("req", 0L);
		long throughput = updMap.getOrDefault("throughput", 0L);
		long rtime = updMap.getOrDefault("rtime", 0L);
		
		this.reqSinceUpdate.setValue(req);
		this.avgResponseTime.setValue(rtime);
		this.avgThroughput.setValue(throughput);
	}
	
	private void parseConfig(String configfile) throws IOException {
		Properties config = new Properties();
		
		config.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(configfile));
		
		String endpoint = config.getProperty("service.endpoint", DEFAULT_SERVICE_ENDPOINT);
		String port = config.getProperty("service.port", DEFAULT_SERVICE_PORT);
		this.service = endpoint + ":" + port + DEFAULT_CATASCOPIA_URI;
		
		System.out.println("endpoint for service probing... " + this.service);
		
		this.headers.add("Accept", "text/html");
		
		for(String h : config.getProperty("service.headers", "").split(";")) {
			String[] header = h.split(":");
			if (header.length == 2)
				this.headers.add(header[0].trim(), header[1].trim());
		}
		
		StringBuffer sb = new StringBuffer("headers for service consumption: ");
		this.headers.forEach((k,v) -> sb.append((k + v) + ";"));
		System.out.println(sb);
	}
	
	private Map<String, Long> getUpdates() {
	    ResponseEntity<String> resp = rest.exchange(this.service, HttpMethod.GET, 
	    											new HttpEntity<String>("", this.headers), 
	    											String.class);
	    
	    Map<String,Long> updMap = new HashMap<String,Long>();
	    
	    if (resp.getStatusCode() == HttpStatus.OK)
	    	Stream.of(resp.getBody().split("\n"))
									.forEach(l -> { String[] arr = l.split("\\|");
												  	if (arr.length == 2)
												  		updMap.put(arr[0], Long.parseLong(arr[1]));
													}
									);
	    return updMap;
	}
}
