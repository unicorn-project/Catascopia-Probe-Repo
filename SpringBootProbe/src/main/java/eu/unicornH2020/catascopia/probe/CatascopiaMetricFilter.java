package eu.unicornH2020.catascopia.probe;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

@Component
public class CatascopiaMetricFilter  implements Filter {

	private static final Logger logger = LoggerFactory.getLogger(CatascopiaMetricFilter.class);

	@Autowired
	private CatascopiaMetricProvider provider;
	
	@Override
	public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws IOException, ServletException {
		long tstart = System.currentTimeMillis();
		HttpServletRequest request = (HttpServletRequest) req;
		
		chain.doFilter(req, resp);

		if (!request.getRequestURI().contains("/catascopia"))
			this.provider.updateStats(System.currentTimeMillis() - tstart);		
	}

	@Override
	public void init(FilterConfig arg0) throws ServletException {
		logger.info("Catascopia for Spring Boot initialized...");
	}

	@Override
	public void destroy() {
		logger.info("Catascopia for Spring Boot stopping gracefully...");
	}
}
