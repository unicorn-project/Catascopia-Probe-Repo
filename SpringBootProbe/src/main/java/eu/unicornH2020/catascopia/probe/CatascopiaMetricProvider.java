package eu.unicornH2020.catascopia.probe;

import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class CatascopiaMetricProvider {
	
	private ConcurrentMap<String, Long>  stats;
	
	public CatascopiaMetricProvider() {
		this.stats = new ConcurrentHashMap<String, Long>();
		this.resetStats();
	}
	
	public void updateStats(long r) {
		this.stats.compute("req", (k,v) -> v += 1);
		this.stats.compute("rtime", (k,v) -> v += r);
	}
	
	public void resetStats() {
		this.stats.put("rtime", 0L);
		this.stats.put("req", 0L);
		this.stats.put("lasttime", System.currentTimeMillis());
	}
	
	public ConcurrentMap<String, Long> getStats() {
		return this.stats;
	}
	
	@RequestMapping(value = "/catascopia",
					method = RequestMethod.GET,
					produces = {"application/json", "text/html"}
				   )
	public String metrics(@RequestParam(name = "raw", defaultValue = "false") boolean raw) {
		
		long diff = System.currentTimeMillis() - this.getStats().get("lasttime");

		long req_since_update = this.getStats().getOrDefault("req", 0L);
		long avg_rtime = 0L;
		long avg_throughput = 0L; 
				
		if (diff > 0 && req_since_update > 0) {
			avg_rtime = this.getStats().get("rtime") / req_since_update;
			avg_throughput = 1000 * req_since_update / diff;
		}
			
		StringBuilder sb = new StringBuilder();
		
		if(raw) {
			sb.append("rtime|" + avg_rtime + "\n");
			sb.append("throughput|" + avg_throughput + "\n");
			sb.append("req|" + req_since_update);

			this.resetStats();
		}
		else {
			sb.append("{\"avg_rtime\":\"" + avg_rtime + "\"");
			sb.append(",\"avg_throughput\":\"" + avg_throughput + "\"");
			sb.append(",\"req_since_update\":\"" + req_since_update + "\"}");
		}
				
		return sb.toString();
    }
}
