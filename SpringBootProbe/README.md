SpringBootProbe
===============
Catascopia Monitoring Probe for reporting monitoring data from apps using Spring Boot framework for java.

Getting Started
---------------
**Step 0**: In the unlikely case you have not already added Catascopia Monitoring to your application, you need to do that first. For detailed step guide follow the steps [here](https://gitlab.com/unicorn-project/uCatascopia-Agent).

**Step 1**. Add the maven dependency for this probe to your project:
```xml
    <!-- spring boot probe for catascopia monitoring -->
    <dependency>
    	<groupId>eu.unicornH2020.catascopia</groupId>
    	<artifactId>SpringBootProbe</artifactId>
    	<version>0.0.1-SNAPSHOT</version>
    </dependency>
```

2. Use the **probe.properties** file to alter the default probe configuration

Available parameters:
- "service.endpoint" (default "http://localhost")
- "service.port" (default "8080")
- "service.headers" (default "") if multiple headers will be appended then they must be delimited by ";" e.g., X-MY_CUSTOM_API_KEY:1234;X-ANOTHER_HEADER:3845fgd85930dkf


3. In your application booter "import" the CatascopiaMetricFilter and CatascopiaMetricProvider and start Catascopia
```java
@SpringBootApplication
@Import({CatascopiaMetricFilter.class, CatascopiaMetricProvider.class})
public class Application {
	
    @UnicornCatascopiaMonitoring
    public static void main(String[] args) {
        SpringApplication.run(Application.class, args);
    }
}
```