package eu.unicornH2020.catascopia.probe;

import eu.unicornH2020.catascopia.probe.Probe;
import eu.unicornH2020.catascopia.probe.exceptions.CatascopiaMetricValueException;
import eu.unicornH2020.catascopia.probe.metricLibrary.SimpleMetric;

import java.lang.management.GarbageCollectorMXBean;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryMXBean;
import java.lang.management.ThreadMXBean;
import java.util.HashMap;
import java.util.List;

public class JVMProbe extends Probe {
	
	private static final int DEFAULT_SAMPLING_PERIOD = 15000;
	private static final String DEFAULT_PROBE_NAME = "JVMProbe";
	
	private static final int KILLOS = 1000;
	private static final int MILLIS = 1000000;
	private static final int NANOS = 1000000000;
	
	private SimpleMetric<Long> heapUsed;
	private SimpleMetric<Long> nonHeapUsed;
	private SimpleMetric<Double> memUsed;
	private SimpleMetric<Double> systemLoad;
	private SimpleMetric<Long> avgGcTime;
	private SimpleMetric<Integer> liveThreads;
	
	private MemoryMXBean memMXBean;
	private ThreadMXBean threadMXBean;
	
	private HashMap<Long,LogEvent> cpuLogMap; 
	private long cpuLastTime;
	private HashMap<String,LogEvent> gcLogMap; 
	
	public JVMProbe(String name, long period) {
		super(name, period);
		
		this.memMXBean = ManagementFactory.getMemoryMXBean();
		this.threadMXBean = ManagementFactory.getThreadMXBean();
		
		this.cpuLogMap = new HashMap<Long,LogEvent>();
		this.cpuLastTime = System.currentTimeMillis();

		this.gcLogMap = new HashMap<String, LogEvent>();
		
		this.heapUsed = new SimpleMetric<Long>("heapUsed", "KB", "current utilized heap memory", false, 0L, this.memMXBean.getHeapMemoryUsage().getMax()/KILLOS);
		this.nonHeapUsed = new SimpleMetric<Long>("nonHeapUsed", "KB", "current utilized non heap memory", false, 0L, this.memMXBean.getNonHeapMemoryUsage().getMax()/KILLOS);
		this.systemLoad = new SimpleMetric<Double>("systemLoad", "%", "current system load on JVM container", false, 0.0, 100.0);
		this.avgGcTime = new SimpleMetric<Long>("avgGcTime", "ms", "average garbage collector time in ms", false, 0L, Long.MAX_VALUE);
		this.memUsed = new SimpleMetric<Double>("memUsed", "%", "current memory utilization for the JVM container", false, 0.0, 100.0);
		this.liveThreads = new SimpleMetric<Integer>("liveThreads", "", "number of live threads", false, 0, Integer.MAX_VALUE);
		
		this.addMetricToProbe(heapUsed);
		this.addMetricToProbe(nonHeapUsed);
		this.addMetricToProbe(systemLoad);
		this.addMetricToProbe(avgGcTime);
		this.addMetricToProbe(memUsed);
		this.addMetricToProbe(liveThreads);
	}
	
	public JVMProbe() {
		this(DEFAULT_PROBE_NAME, DEFAULT_SAMPLING_PERIOD);
	}
	
	@Override
	public String getDescription() {
		return "Probe monitoring the performance of the underlying JVM";
	}
	@Override
	public void collect() throws CatascopiaMetricValueException {
		this.heapUsed.setValue(this.memMXBean.getHeapMemoryUsage().getUsed()/KILLOS);
		this.nonHeapUsed.setValue(this.memMXBean.getNonHeapMemoryUsage().getUsed()/KILLOS);
		
		this.systemLoad.setValue(this.getCPULoad());
		this.liveThreads.setValue(this.cpuLogMap.size());

		this.avgGcTime.setValue(this.getAvgGcTime());
		this.memUsed.setValue(this.getMemUsed());
		
//		System.out.println(this.toJSON());
	}
	
	private double getMemUsed() {
		//total memory available for JVM container might be isolated
		//for this reason find used memory as a percentage towards 
		//the max memory of the container NOT the VM/OS memory
		
		long totalMem = Runtime.getRuntime().totalMemory(); 
		long freeMem  = Runtime.getRuntime().freeMemory();
		
		return ((100.0 * (totalMem - freeMem)) / totalMem); //used JVM memory as percentage
	}
	
	private double getCPULoad() {
		long curTime = System.currentTimeMillis();
		double maxAvailProcTime = (double) (curTime - this.cpuLastTime) / MILLIS * Runtime.getRuntime().availableProcessors(); //in seconds
		
		long[] threads = this.threadMXBean.getAllThreadIds();
		
		for (long tid : threads){
			long curCpu = this.threadMXBean.getThreadCpuTime(tid); //in nanos
			//check if thread is alive
			if (curCpu == -1){ 
				this.cpuLogMap.remove(tid);
				continue;
			}
			LogEvent event = this.cpuLogMap.get(tid);
			//check if thread is already in log
			if (event != null){
				event.prev = event.cur;
				event.cur = curCpu;
			}
			else {
				event = new LogEvent();
				event.prev = curCpu;
				event.cur = curCpu;
				this.cpuLogMap.put(tid, event);
			}
		}
		
		long cpuTotalTime = 0;
		for (LogEvent e : this.cpuLogMap.values()){
			cpuTotalTime += e.cur - e.prev; // in nanos
		}
		
		this.cpuLastTime = curTime;	//in ms
				
		return (cpuTotalTime * 100.0) / (NANOS * maxAvailProcTime);
	}
	
	private long getAvgGcTime() {
		List<GarbageCollectorMXBean> gcList = ManagementFactory.getGarbageCollectorMXBeans();
		
		for (GarbageCollectorMXBean gc : gcList){
			long ti = gc.getCollectionTime();
			
			if (ti == -1){
				this.gcLogMap.remove(gc.getName());
				continue;
			}
			LogEvent event = this.gcLogMap.get(gc.getName());
			//check if gc is already in log
			if (event != null){
				event.prev = event.cur;
				event.cur = ti;
			}
			else {
				event = new LogEvent();
				event.prev = ti;
				event.cur = ti;
				this.gcLogMap.put(gc.getName(), event);
			}
		}
		
		long gcTime = 0;
		for (LogEvent e : this.gcLogMap.values()){
			gcTime += e.cur - e.prev;
		}
		
		return gcTime;
	}
	
	private static class LogEvent {
        long prev;
        long cur;
	}
	
	public static void main(String[] args){
		JVMProbe p = new JVMProbe();
		p.activate();
	}
}
