Catascopia-Probe-Repo
=====================
In this repository you will find probes for Catascopia Monitoring to customize and enrich your apps monitoring process.

Getting Started
---------------
For each probe you will find detailed steps on how to enabled them.

In the unlikely case you have not already added Catascopia Monitoring to your application, you need to do that first. For detailed step guide follow the steps [here](https://gitlab.com/unicorn-project/uCatascopia-Agent).
