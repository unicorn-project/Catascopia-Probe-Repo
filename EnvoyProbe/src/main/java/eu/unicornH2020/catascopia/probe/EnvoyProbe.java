package eu.unicornH2020.catascopia.probe;

import java.io.IOException;
import java.util.Properties;

import eu.unicornH2020.catascopia.probe.exceptions.CatascopiaMetricValueException;
import eu.unicornH2020.catascopia.probe.metricLibrary.SimpleMetric;
import okhttp3.Headers;
import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;
import okio.BufferedSource;

public class EnvoyProbe extends Probe {

	private static final int DEFAULT_SAMPLING_PERIOD = 15000;
	private static final String DEFAULT_PROBE_NAME = "EnvoyProbe";
	
	private static final String DEFAULT_SERVICE_ENDPOINT = "http://localhost";
	private static final String DEFAULT_SERVICE_PORT = "10000";
	private static final String DEFAULT_CONFIG_PATH = "envoyprobe.properties";

	
	private SimpleMetric<Integer> instanceCount;
	private SimpleMetric<Long> upstream_rq_200;
	private SimpleMetric<Long> upstream_rq_2xx;
	private SimpleMetric<Long> upstream_rq_304;
	private SimpleMetric<Long> upstream_rq_3xx;
	private SimpleMetric<Long> upstream_rq_503;
	private SimpleMetric<Long> upstream_rq_5xx;

	private OkHttpClient client;
	private Request request;
	private Headers headers;
	private String serviceURL;
	
	public EnvoyProbe(String name, long period) throws Exception {
		super(name, period);
		
		this.instanceCount = new SimpleMetric<Integer>("instanceCount", "", "reporting number of active instances in cluster", true, 0 , Integer.MAX_VALUE);
		this.upstream_rq_200 = new SimpleMetric<Long>("upstream_rq_200", "", "aggregated 200 reqquests", true, 0L , Long.MAX_VALUE);
		this.upstream_rq_2xx = new SimpleMetric<Long>("upstream_rq_2xx", "", "aggregated 2xx reqquests", true, 0L , Long.MAX_VALUE);
		this.upstream_rq_304 = new SimpleMetric<Long>("upstream_rq_304", "", "aggregated 304 reqquests", true, 0L , Long.MAX_VALUE);
		this.upstream_rq_3xx = new SimpleMetric<Long>("upstream_rq_3xx", "", "aggregated 3xx reqquests", true, 0L , Long.MAX_VALUE);
		this.upstream_rq_503 = new SimpleMetric<Long>("upstream_rq_503", "", "aggregated 503 reqquests", true, 0L , Long.MAX_VALUE);
		this.upstream_rq_5xx = new SimpleMetric<Long>("upstream_rq_5xx", "", "aggregated 5xx reqquests", true, 0L , Long.MAX_VALUE);

		this.upstream_rq_200.setValue(0L);
		this.upstream_rq_2xx.setValue(0L);
		this.upstream_rq_304.setValue(0L);
		this.upstream_rq_3xx.setValue(0L);
		this.upstream_rq_503.setValue(0L);
		this.upstream_rq_5xx.setValue(0L);

		this.addMetricToProbe(this.instanceCount);		
		this.addMetricToProbe(this.upstream_rq_200);		
		this.addMetricToProbe(this.upstream_rq_2xx);		
		this.addMetricToProbe(this.upstream_rq_304);		
		this.addMetricToProbe(this.upstream_rq_3xx);		
		this.addMetricToProbe(this.upstream_rq_503);		
		this.addMetricToProbe(this.upstream_rq_5xx);		

		this.parseConfig(DEFAULT_CONFIG_PATH);
		
		client = new OkHttpClient();
		request = new Request.Builder()
							 .url(this.serviceURL)
							 .headers(this.headers)
							 .build();

	}
	
	public EnvoyProbe() throws Exception {
		this(DEFAULT_PROBE_NAME, DEFAULT_SAMPLING_PERIOD);
	}

	@Override
	public String getDescription() {
		return "Probe reporting metrics for Envoy Load Balancer";
	}

	@Override
	public void collect() throws CatascopiaMetricValueException {
		try (Response response = client.newCall(request).execute()) {
			BufferedSource body = response.body().source();
			
		    String line; 
			while((line = body.readUtf8Line())!= null) {
				
				if (line.contains("membership_healthy:")) {
					this.instanceCount.setValue(Integer.parseInt(line.split(" ")[1]));
					continue;
				}
				
				if (line.contains("upstream_rq_200:")) {
					this.upstream_rq_200.setValue(Long.parseLong(line.split(" ")[1]));
					continue;
				}
				
				if (line.contains("upstream_rq_2xx:")) {
					this.upstream_rq_2xx.setValue(Long.parseLong(line.split(" ")[1]));
					continue;
				}
				
				if (line.contains("upstream_rq_304:")) {
					this.upstream_rq_304.setValue(Long.parseLong(line.split(" ")[1]));
					continue;
				}
				
				if (line.contains("upstream_rq_3xx:")) {
					this.upstream_rq_3xx.setValue(Long.parseLong(line.split(" ")[1]));
					continue;
				}
				
				if (line.contains("upstream_rq_503:")) {
					this.upstream_rq_503.setValue(Long.parseLong(line.split(" ")[1]));
					continue;
				}
				
				if (line.contains("upstream_rq_5xx:")) {
					this.upstream_rq_5xx.setValue(Long.parseLong(line.split(" ")[1]));
					continue;
				}
			}
		} 
		catch (IOException e) {
			e.printStackTrace();
		}
		catch (Exception e) {
			e.printStackTrace();
		}
		
	}
	
	private void parseConfig(String configfile) throws IOException {
		Properties config = new Properties();
		
		config.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(configfile));
		
		String endpoint = config.getProperty("service.endpoint", DEFAULT_SERVICE_ENDPOINT);
		String port = config.getProperty("service.port", DEFAULT_SERVICE_PORT);
		
		this.serviceURL = endpoint + ":" + port + "/stats";
		
		System.out.println("EnvoyProbe>> endpoint for service probing... " + this.serviceURL);
		
		Headers.Builder builder = new Headers.Builder();
		for(String h : config.getProperty("service.headers", "").split(";")) {
			String[] header = h.split(":");
			if (header.length == 2)
				builder.add(header[0].trim(), header[1].trim());
		}
		this.headers = builder.build();
		
	}
	
	public static void main(String[] args) throws Exception {
		EnvoyProbe p = new EnvoyProbe();
		p.activate();
	}
}
