package eu.unicornH2020.catascopia.probe;


import eu.unicornH2020.catascopia.probe.exceptions.CatascopiaMetricValueException;
import eu.unicornH2020.catascopia.probe.metricLibrary.SimpleMetric;
import org.springframework.http.*;
import org.springframework.web.client.RestTemplate;

import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.stream.Stream;

public class BlockChainProbe extends Probe {
	
	private static final int DEFAULT_SAMPLING_PERIOD = 10000;
	private static final String DEFAULT_PROBE_NAME = "BlockChainProbe";

	private Map<String,SimpleMetric> metrics = new HashMap<>();
	private SimpleMetric<Long> countSuccessfulTasks;

	private String service;
	private RestTemplate rest;
	private HttpHeaders headers;

	private static final String DEFAULT_SERVICE_ENDPOINT = "http://localhost";
	private static final String DEFAULT_SERVICE_PORT = "8080";
	private static final String DEFAULT_CATASCOPIA_URI = "/catascopia/blockchain?raw=true";

	public BlockChainProbe(String name, long period) throws CatascopiaMetricValueException, IOException {
		super(name, period);
		this.countSuccessfulTasks = new SimpleMetric<Long>("count_successful_tasks", "#", "Count tasks that are successfully completed",
				true,0L, Long.MAX_VALUE);

		this.addMetricToProbe(this.countSuccessfulTasks);
		this.metrics.putIfAbsent("count_successful_tasks",this.countSuccessfulTasks);
		this.countSuccessfulTasks.setValue(0);
		this.rest = new RestTemplate();
		this.headers = new HttpHeaders();
		this.parseConfig("probe.properties");
	}
	
	public BlockChainProbe() throws CatascopiaMetricValueException, IOException {
		this(DEFAULT_PROBE_NAME, DEFAULT_SAMPLING_PERIOD);
	}
	
	@Override
	public String getDescription() {
		return "An exemplary probe showcasing the offered functionality";
	}

	@Override
	public void collect() {

		Map<String, Double> updMap = this.getUpdates();

		updMap.entrySet().stream().forEach(entry->{
			String key = entry.getKey();
			Double value = entry.getValue();

			//Do we have this metric?
			SimpleMetric<Double> avg = this.metrics.get(key);

			if(avg == null){
				avg = new SimpleMetric(key,"s","The time taken for executing tasks",false,0,Double.MAX_VALUE);
				try {
					avg.setValue(value);
				} catch (CatascopiaMetricValueException e) {
					e.printStackTrace();
				}
				this.addMetricToProbe(avg);
				System.out.println(avg +" registered");
				this.metrics.putIfAbsent(key,avg);
			}else {
				try {
					avg.setValue(value);
				} catch (CatascopiaMetricValueException e) {
					e.printStackTrace();
				}
			}

		});

	}
	private void parseConfig(String configfile) throws IOException {
		Properties config = new Properties();

		config.load(Thread.currentThread().getContextClassLoader().getResourceAsStream(configfile));

		String endpoint = config.getProperty("service.endpoint", DEFAULT_SERVICE_ENDPOINT);
		String port = config.getProperty("service.port", DEFAULT_SERVICE_PORT);
		this.service = endpoint + ":" + port + DEFAULT_CATASCOPIA_URI;

		System.out.println("endpoint for service probing... " + this.service);

		this.headers.add("Accept", "text/html");

		for(String h : config.getProperty("service.headers", "").split(";")) {
			String[] header = h.split(":");
			if (header.length == 2)
				this.headers.add(header[0].trim(), header[1].trim());
		}

		StringBuffer sb = new StringBuffer("headers for service consumption: ");
		this.headers.forEach((k,v) -> sb.append((k + v) + ";"));
		System.out.println(sb);
	}


	private Map<String, Double> getUpdates() {
		ResponseEntity<String> resp = rest.exchange(this.service, HttpMethod.GET,
				new HttpEntity<>("", this.headers),
				String.class);

		Map<String,Double> updMap = new HashMap<>();

		if (resp.getStatusCode() == HttpStatus.OK) {
			try {
				Stream.of(resp.getBody().split("\n"))
						.forEach(l -> {
									String[] arr = l.split("\\|");
									if (arr.length == 2)
										updMap.put(arr[0], Double.parseDouble(arr[1]));
								}
						);
			} catch (NullPointerException ex) {

				//No metrics
			}
		}
		return updMap;
	}
	public static void main(String[] args) throws CatascopiaMetricValueException, IOException {
		BlockChainProbe p = new BlockChainProbe();
		p.setMetricsAggregatable(false);
		p.activate();
	}

}
